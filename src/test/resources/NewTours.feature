Feature: Login Action
 
@Sanity
Scenario: Successful Login with Valid Credentials
 Given User is on login Page
 When User Navigate to LogIn Page
 And User enters UserName and Password
And click submit 
 Then Message displayed Login Successfully

@Regression

Scenario: Successful Flight Booking 
 Given User is on Flight Booking Page
 When User Navigate to Flight Booking Page
 And Selecting the trip type
 And Selecting the passengers
And Selecting the departing from
And Selecting On Month
And Selecting On Date
And Selecting Arriving in
And Selecting Returning Month
And Selecting Returning Date
And Selecting the service class
And Selecting the airline
And Capturing the screenshot
And Clicking on continue
And Capturing the screenshot
And Clicking on continue in second page
And Entering the first name
And Entering the last name 
And Entering the Number
And Capturing the screenshot
And Clicking on secure purchase
And Checking the confirmation message

 Then Message displayed Flight Booked Successfully