package runners;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions

(features = "F:\\workspace\\Selenium\\NewTours\\src\\test\\resources\\NewTours.feature", tags = {
		"@Sanity,@Regression" }, glue = { "StepDefinition" },

		monochrome = true)

public class TestRunnerNewTour {

}
