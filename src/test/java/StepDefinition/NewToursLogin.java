package StepDefinition;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.NewTours.CommonMethods.NewToursMethods;
import com.NewTours.ExcelUtilities.ExcelUtilities;
import com.NewTours.WebLocators.NewToursLocators;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NewToursLogin {

	

	@Given("^User is on login Page$")
	public void user_is_on_login_Page() throws Throwable {
		File dir1 = new File("TC1_Screenshots"); // Specify the Folder name here
		dir1.mkdir();
		System.setProperty("webdriver.chrome.driver", "F:\\Rosy\\Drivers2019\\chromedriver\\chromedriver.exe");

		WebDriver driver = NewToursMethods.getWebDriver();

		driver.manage().window().maximize();
		System.out.println("Browser initialized");
		ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 1, 0, 0, "Step1");
		ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 1, 1, 0,
				"Browser_Initialized");
		ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 1, 2, 0, "Passed");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@When("^User Navigate to LogIn Page$")
	public void user_Navigate_to_LogIn_Page() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		try {
			driver.get(
					ExcelUtilities.getURL("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 0, "URL"));
			System.out.println("Login page is displayed");
			NewToursMethods.captureScreenShot(driver, "Step2_Login_Page_Displayed", "TC1_Screenshots");
			ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 1, 0, 1,
					"Step2_Login_page_displayed");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception undergone");
		}
	}

	@When("^User enters UserName and Password$")
	public void user_enters_UserName_and_Password() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		NewToursLocators.txt_userName(driver).sendKeys(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 0, 1, 1));
		System.out.println("Username entered");
		NewToursLocators.txt_password(driver).sendKeys(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 0, 1, 2));
		System.out.println("Password entered");
		NewToursMethods.captureScreenShot(driver, "Step3_Username_Password_entered", "TC1_Screenshots");
	}

	@When("^click submit$")
	public void click_submit() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		NewToursLocators.btn_SignIn(driver).click();
		Thread.sleep(10000);
		System.out.println("Sign in button clicked");
		ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 2, 0, 1,
				"Step3_Signin_button_clicked");
	}

	@Then("^Message displayed Login Successfully$")
	public void message_displayed_Login_Successfully() throws Throwable {
		System.out.println("login successfully displayed");
	}

}
