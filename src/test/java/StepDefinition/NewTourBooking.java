package StepDefinition;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.NewTours.CommonMethods.NewToursMethods;
import com.NewTours.ExcelUtilities.ExcelUtilities;
import com.NewTours.WebLocators.NewToursHomepageLocators;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NewTourBooking {
	

	@Given("^User logged in successfully$")
	public void user_logged_in_successfully() throws Throwable {
		
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Yes user logged in successfully");
	}

	@Given("^User is on Flight Booking Page$")
	public void user_is_on_Flight_Booking_Page() throws Throwable {
		System.out.println("on Flight Booking Page");

	}

	@When("^User Navigate to Flight Booking Page$")
	public void user_Navigate_to_Flight_Booking_Page() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		NewToursMethods.captureScreenShot(driver, "Step4_Homepage_Displayed", "TC1_Screenshots");
		ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 3, 0, 1,
				"Step4_Home_Page_Displayed");

	}

	@When("^Selecting the trip type$")
	public void selecting_the_trip_type() throws Throwable {
		String TripType = ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 0);
		WebDriver driver = NewToursMethods.getWebDriver();
		if (TripType == "Round Trip") {
			driver.findElement(By.xpath(NewToursHomepageLocators.RoundTrip_XPATH)).click();
		} else {
			driver.findElement(By.xpath(NewToursHomepageLocators.Oneway_XPATH)).click();
		}
	}

	@When("^Selecting the passengers$")
	public void selecting_the_passengers() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select Dropdown = new Select(driver.findElement(By.name(NewToursHomepageLocators.Passengers_NAME)));
		Dropdown.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 1));

	}

	@When("^Selecting the departing from$")
	public void selecting_the_departing_from() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select Departingfrom = new Select(driver.findElement(By.name(NewToursHomepageLocators.Departingfrom_NAME)));
		Departingfrom.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 2));

	}

	@When("^Selecting On Month$")
	public void selecting_On_Month() throws IOException {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select Month = new Select(driver.findElement(By.name(NewToursHomepageLocators.OnMonth_NAME)));
		Month.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 3));
	}

	@When("^Selecting On Date$")
	public void selecting_On_Date() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select Ondate = new Select(driver.findElement(By.name(NewToursHomepageLocators.OnDate_NAME)));
		Ondate.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 4));

	}

	@When("^Selecting Arriving in$")
	public void selecting_Arriving_in() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select arrivein = new Select(driver.findElement(By.name(NewToursHomepageLocators.ArrivingIn_NAME)));
		arrivein.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 5));

	}

	@When("^Selecting Returning Month$")
	public void selecting_Returning_Month() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select returnmonth = new Select(driver.findElement(By.name(NewToursHomepageLocators.ReturningMonth_NAME)));
		returnmonth.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 6));

	}

	@When("^Selecting Returning Date$")
	public void selecting_Returning_Date() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select returndate = new Select(driver.findElement(By.name(NewToursHomepageLocators.ReturningDate_NAME)));
		returndate.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 7));

	}

	@When("^Selecting the service class$")
	public void selecting_the_service_class() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		String ClassType = ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1,
				8);

		if (ClassType == "Business class") {
			driver.findElement(By.cssSelector(NewToursHomepageLocators.BusinessClass_CSSSELECTOR)).click();
		}

		else if (ClassType == "Economy") {
			driver.findElement(By.cssSelector(NewToursHomepageLocators.EconomyClass_CSSSELECTOR)).click();
		}

		else {
			driver.findElement(By.cssSelector(NewToursHomepageLocators.FirstClass_CSSSELECTOR)).click();
		}

	}

	@When("^Selecting the airline$")
	public void selecting_the_airline() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		Select airline = new Select(driver.findElement(By.name(NewToursHomepageLocators.Airline_NAME)));

		airline.selectByVisibleText(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 9));

	}

	@When("^Capturing the screenshot$")
	public void capturing_the_screenshot() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		NewToursMethods.captureScreenShot(driver, "Step5_First_page_details_filled", "TC1_Screenshots");
		ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 4, 0, 1,
				"Step5_First_Page_Details_filled");

	}

	@When("^Clicking on continue$")
	public void clicking_on_continue() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		driver.findElement(By.name(NewToursHomepageLocators.Continue_NAME)).click();

	}

	@When("^Clicking on continue in second page$")
	public void clicking_on_continue_in_second_page() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		driver.findElement(By.name(NewToursHomepageLocators.Continue1_NAME)).click();

	}

	@When("^Entering the first name$")
	public void entering_the_first_name() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		driver.findElement(By.name(NewToursHomepageLocators.FirstName_NAME)).sendKeys(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 10));

	}

	@When("^Entering the last name$")
	public void entering_the_last_name() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		driver.findElement(By.name(NewToursHomepageLocators.LastName_NAME)).sendKeys(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 11));

	}

	@When("^Entering the Number$")
	public void entering_the_Number() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		driver.findElement(By.name(NewToursHomepageLocators.Number_NAME)).sendKeys(
				ExcelUtilities.getData("F:\\workspace\\Selenium\\NewTours\\TestData\\TestData.xlsx", 1, 1, 12));

	}

	@When("^Clicking on secure purchase$")
	public void clicking_on_secure_purchase() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		driver.findElement(By.name(NewToursHomepageLocators.Securepurchase_NAME)).click();

	}

	@When("^Checking the confirmation message$")
	public void checking_the_confirmation_message() throws Throwable {
		WebDriver driver = NewToursMethods.getWebDriver();
		String ConfirmTitle;
		ConfirmTitle = "Flight Confirmation: Mercury Tours";
		boolean flag = false;
		if (driver.getTitle().equalsIgnoreCase(ConfirmTitle)) {
			flag = true;
			System.out.println("Flight Booked Successfully");
			NewToursMethods.captureScreenShot(driver, "Step8_Flight_Book_Confirmation_Page", "TC1_Screenshots");
			// NewToursMethods.captureScreenShot(driver,
			// "Step8_Flight_Book_Confirmation_Page","TC1_Screenshots");
			ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 1, 2, 0, "Passed");
			ExcelUtilities.writeresult("F:\\workspace\\Selenium\\NewTours\\TestData\\Results.xlsx", 7, 0, 1,
					"Step8_Flight_Booking_Confirmed");
		}
		Assert.assertTrue(flag, "Page title is not matching with expected");
	}

	@Then("^Message displayed Flight Booked Successfully$")
	public void message_displayed_Flight_Booked_Successfully() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

	}

}
