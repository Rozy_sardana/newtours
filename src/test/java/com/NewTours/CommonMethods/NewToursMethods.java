package com.NewTours.CommonMethods;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NewToursMethods {
	private static WebDriver driver;
	public static void captureScreenShot(WebDriver ldriver, String FileName, String folder) throws IOException {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) ldriver).getScreenshotAs(OutputType.FILE);
		try {
			// now copy the screenshot to desired location using copyFile method
			FileUtils.copyFile(src,
					new File("F:\\workspace\\Selenium\\NewTours\\" + folder + "\\" + FileName + ".png"));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static WebDriver getWebDriver() {
		if(NewToursMethods.driver == null) {
			NewToursMethods.driver = new ChromeDriver();
		}
		return NewToursMethods.driver;
	}
}
